from django import forms

from django.contrib.auth.models import User
from .models import *
from django import forms


class AddAdvertForm(forms.ModelForm):
    class Meta:
        model = Mountains
        fields = '__all__'


class AdvertImagesForm(forms.Form):

    photos = forms.FileField(widget=forms.FileInput(attrs={'multiple': True}))

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        super(AdvertImagesForm, self).__init__(*args, **kwargs)

    def clean_photos(self):
        # Остаются только картинки
        photos = [photo for photo in self.request.FILES.getlist('photos') if 'image' in photo.content_type]
        # Если среди загруженных файлов картинок нет, то исключение
        if len(photos) == 0:
            raise forms.ValidationError(u'Not found uploaded photos.')
        return photos

    def save_for(self, advert):
        for photo in self.cleaned_data['photos']:
            AdvertImage(photo=photo, advert=advert).save()

class UploadFileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('user', 'avatar')

class LoginForm(forms.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = 'Логін'
        self.fields['password'].label = 'Пароль'

    def clean(self):
        password = self.cleaned_data['password']
        username = self.cleaned_data['username']

        if not User.objects.filter(username=username).exists():
            raise forms.ValidationError(f'Користувача з логіном {username} не було знайдено в системі')
        user = User.objects.filter(username=username).first()
        if user:
            if not user.check_password(password):
                raise forms.ValidationError(f'Неправильний пароль')
            return self.cleaned_data

    class Meta:
        model = User
        fields = {'username','password'}

class RegistrationForm(forms.ModelForm):

    confirm_password = forms.CharField(widget=forms.PasswordInput)
    password = forms.CharField(widget=forms.PasswordInput)
    phone = forms.CharField(required=False)
    email = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = 'Логін'
        self.fields['password'].label = 'Пароль'
        self.fields['confirm_password'].label = 'Підтвердіть пароль'
        self.fields['phone'].label = 'Номер телефону'
        self.fields['first_name'].label = 'Ім`я'
        self.fields['email'].label = 'Email'

    def clean_email(self):
        email = self.cleaned_data['email']
        domain = email.split('.')[-1]
        if domain in ['net']:
           raise forms.ValidationError(f'Реєстрація для домену "{domain}" неможлива')
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError(f'Користувач з електронною адресою {email} вже існує')
        return email

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.filter(username=username).exists():
            raise forms.ValidationError(f'Ім`я користувача "{username}" зайнято')
        return username

    def clean(self):
        password = self.cleaned_data['password']
        confirm_password = self.cleaned_data['confirm_password']
        if password != confirm_password:
            raise forms.ValidationError(f'Паролі не збігаються')
        return self.cleaned_data

    class Meta:
        model = User
        fields = ['username', 'password', 'confirm_password', 'first_name', 'phone', 'email']
