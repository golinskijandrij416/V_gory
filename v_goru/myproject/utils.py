from django.db import models

def recalc_cart(cart):
    cart_data = cart.products.aggregate( models.Count('id'))
    print(cart_data)
    cart.total_products = cart_data['id__count']
    cart.save()
