from PIL import Image
from django.db import models
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
import os
from django.utils import timezone
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey




User = get_user_model()


# Create your models here.
def get_mountains_url(obj, viewname):
    ct_model = obj.__class__._meta.model_name
    return reverse(viewname, kwargs={'ct_model': ct_model, 'slug':obj.slug})

def get_product_url(obj, viewname):
        ct_model = obj.__class__._meta.model_name
        return reverse(viewname, kwargs={'ct_model': ct_model, 'slug': obj.slug})

class UserProfile(models.Model):
    user = models.OneToOneField(User,  on_delete=models.CASCADE,
        primary_key=True, default=['username'])
    avatar = models.ImageField() # or whatever

    def __str__(self):
        return self.user.username



class LatestProductsManager:
    @staticmethod
    def get_products_for_main_page(*args, **kwargs):
        with_respect_to = kwargs.get('with_respect_to')
        products = []
        ct_models = ContentType.objects.filter(model__in=args)
        for ct_model in ct_models:
            model_products = ct_model.model_class()._base_manager.all().order_by('-id')[:100]
            products.extend(model_products)
        if with_respect_to:
            ct_model = ContentType.objects.filter(model=with_respect_to)
            if ct_model.exists():
                if with_respect_to in args:
                    return sorted(
                        products, key=lambda x: x.__class__._meta.model_name.startswith(with_respect_to),
                        reverse=True
                    )
        return products


class LatestProducts:
    objects = LatestProductsManager()


class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name='Назва категорії')
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category_detail', kwargs={'slug': self.slug})


class Slug(models.Model):

    class Meta:
        abstract = True

    category = models.ForeignKey(Category, verbose_name='Категорія', on_delete=models.CASCADE)
    slug = models.SlugField(unique=True)

class Oblast(Slug):

    name = models.CharField(max_length=255, verbose_name='Назва області')

    def __str__(self):
        return "{} ".format(self.name)


class Mountains(Slug):
    description = models.TextField(max_length = 1000, verbose_name='Опис')
    obl = models.ForeignKey('Oblast',on_delete=models.CASCADE, verbose_name='Область в якій знаходиться')
    name = models.CharField(max_length=255, verbose_name='Назва гори')
    image = models.ImageField(verbose_name='Зображення')
    image_panorama = models.ImageField(verbose_name='Зображення 360')
    height = models.DecimalField(max_digits=4, decimal_places=0, verbose_name='Висота гори')
    misznahod = models.CharField(max_length=255, verbose_name='Місцезнаходження')
    marsrut = models.TextField(max_length=500, verbose_name='Маршрути з міст/сіл')
    top_marsrut = models.CharField(max_length=255, verbose_name='Найкращий маршрут і відстань')
    sposib_peresuv = models.CharField(max_length=300, verbose_name='Спосіб пересування', default='Пішки')
    infrastructura = models.TextField(max_length=255, verbose_name='Готелі,кафе,бази відпочинку поблизу')
    riv_sklad = models.CharField(max_length=255, verbose_name='Рівень складності')

    def __str__(self):
        return "{} ".format(self.name)


    def get_model_name(self):
        return self.__class__.__name__.lower()

    def get_absolute_url(self):
        return get_mountains_url(self, 'product_detail')


class UploadFile(models.Model):
    title = models.CharField(max_length = 50, blank=True)
    image = models.ImageField(upload_to='media')


class AdvertImage(models.Model):
    advert = models.ForeignKey(Mountains, on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='uploads/')

    def __str__(self):
        return "{} ".format(self.advert)

class Blog(Slug):
    title = models.CharField(max_length=255, verbose_name='Заголовок')
    body = models.TextField(max_length=1000, verbose_name='Текст блогу')
    image = models.ImageField(verbose_name='Зображення',default='Пішки')

    def __str__(self):
        return "{} ".format(self.title)


class CartProduct(models.Model):

    user = models.ForeignKey('Customer', verbose_name='Покупець', on_delete=models.CASCADE)
    cart = models.ForeignKey('Cart', verbose_name='Вполобані гори', on_delete=models.CASCADE, related_name='related_products')
    product = models.ForeignKey(Mountains, verbose_name='Товар', on_delete=models.CASCADE)

    def __str__(self):
        return "Продук: {}(для корзини)".format(self.product.title)


class Cart(models.Model):

    owner = models.ForeignKey('Customer', null=True,verbose_name='Власник', on_delete=models.CASCADE)
    products = models.ManyToManyField(CartProduct, blank=True, related_name='related_cart')
    total_products = models.PositiveIntegerField(default=0)
    for_anonymous_user = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)

    def get_model_name(self):
        return self.__class__.__name__.lower()


class Customer(models.Model):
    user = models.ForeignKey(User, verbose_name='Користувач', on_delete=models.CASCADE)
    phone = models.CharField(max_length=255, verbose_name='Номер телефона', null=True, blank=True)

    def __str__(self):
        return "Покупець: {} {}".format( self.user.first_name, self.user.last_name)
