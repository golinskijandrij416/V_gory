from django.contrib import admin
from .models import *



admin.site.register(UserProfile)
admin.site.register(Category)
admin.site.register(Oblast)
admin.site.register(Mountains)
admin.site.register(Blog)
admin.site.register(AdvertImage)
