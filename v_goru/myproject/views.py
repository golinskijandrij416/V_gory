from django.views.generic import ListView, DetailView, View
from django.shortcuts import render
from django.contrib.auth import authenticate,login
from django.http import HttpResponseRedirect, JsonResponse
from .forms import *
from .models import *
from django.contrib import messages
from django.urls import reverse_lazy
from django.contrib.auth.views import PasswordChangeView, PasswordResetView, \
PasswordResetConfirmView, PasswordResetDoneView, PasswordChangeView,\
 PasswordChangeDoneView, PasswordResetCompleteView
from django.contrib.auth.decorators import login_required
from .mixins import CartMixin
from .utils import *

class CreateAdvertView(View):
    template_name = 'index.html'
    form_class = AddAdvertForm
    form_images_class = AdvertImagesForm
    model = Mountains

    def get(self, request):
        form = self.form_class()
        form_images = self.form_images_class()
        return render(request, self.template_name, {'form': form, 'form_images': form_images})

    def post(self, request):
        form = self.form_class(request.POST)
        form_images = self.form_images_class(request.POST, request.FILES, request=request)
        if form.is_valid() and form_images.is_valid():
            advert = form.save()
            form_images.save_for(advert)
            return HttpResponseRedirect('/')
        return render(request, self.template_name, {'form': form, 'form_images': form_images})

class PasswordChangeView(PasswordChangeView):
  template_name = 'change_by_user.html'
  success_url = '/'


class BaseView(CartMixin,View):
    def get(self, request, *args, **kwargs):

        products = LatestProducts.objects.get_products_for_main_page('mountains','blog')
        context = {
            'products': products,
            'mountains': Mountains,
            'oblast':Oblast,
            'blog':Blog,
            'cart': self.cart
        }
        return render(request, 'base.html', context)



class MountainsDetailView(DetailView):

    CT_MODEL_MODEL_CLASS = {
        'mountains': Mountains,

    }
    def dispatch(self, request, *args, **kwargs):
        self.model = self.CT_MODEL_MODEL_CLASS[kwargs['ct_model']]
        self.queryset = self.model._base_manager.all()
        return  super().dispatch(request, *args, **kwargs)


    context_object_name = 'mountains'
    template_name = 'mountains_detail.html'
    slug_url_kwarg = 'slug'


class OblastDetailView(DetailView):


    CT_MODEL_MODEL_CLASS = {
        'oblast': Oblast

    }
    def dispatch(self, request, *args, **kwargs):
        self.model = self.CT_MODEL_MODEL_CLASS[kwargs['ct_model']]
        self.queryset = self.model._base_manager.all()
        return  super().dispatch(request, *args, **kwargs)


    context_object_name =  'oblast'
    template_name = 'oblast_detail.html'
    slug_url_kwarg = 'slug'


class BlogDetailView(DetailView):


    CT_MODEL_MODEL_CLASS = {
        'blog': Blog

    }
    def dispatch(self, request, *args, **kwargs):
        self.model = self.CT_MODEL_MODEL_CLASS[kwargs['ct_model']]
        self.queryset = self.model._base_manager.all()
        return  super().dispatch(request, *args, **kwargs)


    context_object_name =  'blog'
    template_name = 'blog_detail.html'
    slug_url_kwarg = 'slug'


class LoginView(View):
    def get(self, request, *args, **kwargs):
        form = LoginForm(request.POST or None)
        categories = Category.objects.all()
        context = {'form':form, 'categories': categories}
        return render(request, 'accounts/login.html', context)

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST or None)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return HttpResponseRedirect('/')
        context = {'form': form}
        return render(request, 'accounts/login.html', context)


class RegistrationView(CartMixin, View):

    def get(self, request, *args, **kwargs):
        form = RegistrationForm(request.POST or None)
        categories = Category.objects.all()
        context = {
            'form': form,
            'categories': categories,
            'cart': self.cart
        }
        return render(request, 'accounts/registrations.html', context)

    def post(self, request, *args, **kwargs):
        form = RegistrationForm(request.POST or None)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.username = form.cleaned_data['username']
            new_user.email = form.cleaned_data['email']
            new_user.first_name = form.cleaned_data['first_name']
            new_user.save()
            new_user.set_password(form.cleaned_data['password'])
            new_user.save()
            Customer.objects.create(
                user=new_user,
                phone=form.cleaned_data['phone'],
            )
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            login(request, user)
            return HttpResponseRedirect('/')
        context = {'form': form, 'cart': self.cart}
        return render(request, 'accounts/registrations.html', context)



class AddToCartView(CartMixin,View):

    def get(self, request, *args, **kwargs):
        product_slug = kwargs.get('slug')
        product = Mountains.objects.get(slug=product_slug)
        cart_product, created = CartProduct.objects.get_or_create(
            user=self.cart.owner,
            cart=self.cart,
            product = product

        )

        if created:
            self.cart.products.add(cart_product)
            messages.add_message(request, messages.INFO, "Додано в обрані")
            recalc_cart(self.cart)
            return HttpResponseRedirect('/favorite/')
        else :
            return render(request, 'base.html')


class DeleteFromCartView(CartMixin,View):

    def get(self, request, *args, **kwargs):
        product_slug = kwargs.get('slug')
        product = Mountains.objects.get(slug=product_slug)
        cart_product = CartProduct.objects.get(
            user=self.cart.owner,
            cart=self.cart,
            product = product

        )
        self.cart.products.remove(cart_product)
        cart_product.delete()
        recalc_cart(self.cart)
        messages.add_message(request, messages.INFO, "Видалено з обраних")
        return HttpResponseRedirect('/favorite/')

class CartView(CartMixin,View):

    def get(self, request, *args, **kwargs):

        categories = Category.objects.all()
        context = {
          'cart': self.cart,
          'categories': categories

          }
        return render(request, 'cart.html', context)


def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
    else:
        form = UploadFileForm()
    return render(request, 'index.html', {'form': form})

def activate(request, uidb64, token):
    User = get_user_model()
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')
