# Generated by Django 3.1.6 on 2021-03-20 11:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('myproject', '0011_auto_20210319_2346'),
    ]

    operations = [
        migrations.CreateModel(
            name='VenueImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='images\\venue_profiles/%Y/%m/%d')),
            ],
        ),
        migrations.AddField(
            model_name='mountains',
            name='description',
            field=models.TextField(default=0, max_length=1000, verbose_name='Опис'),
        ),
        migrations.CreateModel(
            name='Venue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('images', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myproject.venueimage')),
            ],
        ),
    ]
