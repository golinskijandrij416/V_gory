from django.contrib.auth.views import LogoutView
from django.urls import path, reverse_lazy
from .views import *
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth.views import PasswordChangeView, PasswordResetView
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from myproject import views


urlpatterns = [
    path('', BaseView.as_view(), name='base'),
    path('favorite/', CartView.as_view(), name='cart'),
    path('add_to_cart/<str:slug>/', AddToCartView.as_view(), name='add_to_cart'),
    path('remove-from-cart/<str:slug>/', DeleteFromCartView.as_view(), name='delete_from_cart'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(next_page="/"), name='logout'),
    path('registration/', RegistrationView.as_view(), name='registration'),
    path('password_change/done/', PasswordChangeView.as_view(template_name='change_by_done.html'), name='reloada'),
    path('password-change/', auth_views.PasswordChangeView.as_view(success_url=reverse_lazy('login'),template_name='change_by_user.html'), name='password_change'),
    path('social-auth/', include('social_django.urls', namespace="social")),
    path('password-reset/', views.PasswordResetView.as_view(template_name='accounts/password_reset_form.html'), name='password_reset'),
path('password-reset/done/', views.PasswordResetDoneView.as_view(template_name='accounts/password_reset_done.html'), name='password_reset_done'),
path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(template_name='accounts/password_reset_confirm.html'), name='password_reset_confirm'),
path('reset/done/', views.PasswordResetCompleteView.as_view(template_name='accounts/password_reset_complete.html'), name='password_reset_complete'),
 path('up/', upload_file, name='sad'),
 path('mo/', MountainsDetailView.as_view(), name='a'),
  path('select_mountains/str:ct_model>/<str:slug/', MountainsDetailView.as_view(), name='product_detail'),
]
